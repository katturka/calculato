import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class CalculatorTestsConfiguration {

    public static WebDriver driver = new ChromeDriver();

    public static void config() {

        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
        driver.get("https://web2.0calc.com/");
        driver.manage().window().maximize();
        driver.findElement(By.xpath("//p[contains(.,'Consent')]")).click();
    }
}

