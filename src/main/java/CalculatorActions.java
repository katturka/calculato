public class CalculatorActions {

    public static void clickEquals() {
        CalculatorParameters.buttonEquals.click();
    }

    public static void clickPlus() {
        CalculatorParameters.buttonPlus.click();
    }

    public static void clickMinus() {
        CalculatorParameters.buttonMinus.click();
    }

    public static void clickDivide() {
        CalculatorParameters.buttonDivision.click();
    }

    public static void clickClear() {
        CalculatorParameters.buttonClear.click();
    }

    public static void clickMultiply() {
        CalculatorParameters.buttonMultiplication.click();
    }

    public static void clickCos() {
        CalculatorParameters.buttonMathCos.click();
    }

    public static void clickPi() {
        CalculatorParameters.buttonMathPi.click();
    }

    public static void clickSqrt() {
        CalculatorParameters.buttonMathSqrt.click();
    }

    public static void clickRadian() {
        CalculatorParameters.radioButtonRadian.click();
    }
}
