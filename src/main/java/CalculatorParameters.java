import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

public class CalculatorParameters {

    public static WebElement input = CalculatorTestsConfiguration.driver.findElement(By.id("input"));
    public static WebElement inputValue = CalculatorTestsConfiguration.driver.findElement(By.id("result"));

    public static WebElement buttonEquals = CalculatorTestsConfiguration.driver.findElement(By.id("BtnCalc"));
    public static WebElement buttonPlus = CalculatorTestsConfiguration.driver.findElement(By.id("BtnPlus"));
    public static WebElement buttonMinus = CalculatorTestsConfiguration.driver.findElement(By.id("BtnMinus"));
    public static WebElement buttonDivision = CalculatorTestsConfiguration.driver.findElement(By.id("BtnDiv"));
    public static WebElement buttonClear = CalculatorTestsConfiguration.driver.findElement(By.id("BtnClear"));
    public static WebElement buttonMultiplication = CalculatorTestsConfiguration.driver.findElement(By.id("BtnMult"));

    public static WebElement numberButtonZero = CalculatorTestsConfiguration.driver.findElement(By.id("Btn0"));
    public static WebElement numberButtonOne = CalculatorTestsConfiguration.driver.findElement(By.id("Btn1"));
    public static WebElement numberButtonTwo = CalculatorTestsConfiguration.driver.findElement(By.id("Btn2"));
    public static WebElement numberButtonThree = CalculatorTestsConfiguration.driver.findElement(By.id("Btn3"));
    public static WebElement numberButtonFour = CalculatorTestsConfiguration.driver.findElement(By.id("Btn4"));
    public static WebElement numberButtonFive = CalculatorTestsConfiguration.driver.findElement(By.id("Btn5"));
    public static WebElement numberButtonSix = CalculatorTestsConfiguration.driver.findElement(By.id("Btn6"));
    public static WebElement  numberButtonSeven = CalculatorTestsConfiguration.driver.findElement(By.id("Btn7"));
    public static WebElement numberButtonEight = CalculatorTestsConfiguration.driver.findElement(By.id("Btn8"));
    public static WebElement numberButtonNine = CalculatorTestsConfiguration.driver.findElement(By.id("Btn9"));


    public static WebElement buttonParanLSign = CalculatorTestsConfiguration.driver.findElement(By.id("BtnParanL"));
    public static WebElement buttonParanRSign = CalculatorTestsConfiguration.driver.findElement(By.id("BtnParanR"));

    public static WebElement buttonMathCos = CalculatorTestsConfiguration.driver.findElement(By.id("BtnCos"));
    public static WebElement buttonMathPi = CalculatorTestsConfiguration.driver.findElement(By.id("BtnPi"));
    public static WebElement buttonMathSqrt = CalculatorTestsConfiguration.driver.findElement(By.id("BtnSqrt"));


    public static WebElement radioButtonRadian = CalculatorTestsConfiguration.driver.findElement(By.id("trigorad"));

    public static WebElement buttonHistory = CalculatorTestsConfiguration.driver.findElement(By.id("hist"));
}
