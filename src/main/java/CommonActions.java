import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import java.util.List;

public class CommonActions {

    public static void checkAndClearedIfNeeded() {
        if (!CalculatorParameters.input.getAttribute("value").isEmpty())
            CalculatorActions.clickClear();
    }

    public static void clickHistory() {
        CalculatorParameters.buttonHistory.click();
    }

    public static boolean checkEquationHistory(String... usedEquations) {
        int equals = 0;
        List<WebElement> historyEquationValue = CalculatorTestsConfiguration.driver.findElements(By.xpath("//div[@id='histframe']/..//ul//li/p[@class='l']"));
        for (WebElement element : historyEquationValue) {
            for (String usedEquation : usedEquations) {
                if (element.getAttribute("title").contains(usedEquation)) {
                    equals++;
                }
            }
        }
        if (equals == usedEquations.length) {
            return true;
        } else return false;
    }
}