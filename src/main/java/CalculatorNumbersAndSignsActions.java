import net.jodah.failsafe.internal.util.Assert;

public class CalculatorNumbersAndSignsActions {

    private static final int ZERO = 0;
    private static final int ONE = 1;
    private static final int TWO = 2;
    private static final int THREE = 3;
    private static final int FOUR = 4;
    private static final int FIVE = 5;
    private static final int SIX = 6;
    private static final int SEVEN = 7;
    private static final int EIGHT = 8;
    private static final int NINE = 9;


    public static void clickNumber(String number) {
        Assert.isTrue(number.length() == ONE, "given " + number + " is incorrect");

        try {
            clickNumber(Integer.parseInt(number));
        } catch (
                NumberFormatException ex) {
            ex.printStackTrace();
        }
    }

    public static void clickNumber(int number) {
        Assert.isTrue(number >= ZERO ||number <= NINE, "given " + number + " is incorrect");

        switch (number) {
            case ZERO -> CalculatorParameters.numberButtonZero.click();
            case ONE -> CalculatorParameters.numberButtonOne.click();
            case TWO -> CalculatorParameters.numberButtonTwo.click();
            case THREE -> CalculatorParameters.numberButtonThree.click();
            case FOUR -> CalculatorParameters.numberButtonFour.click();
            case FIVE -> CalculatorParameters.numberButtonFive.click();
            case SIX -> CalculatorParameters.numberButtonSix.click();
            case SEVEN -> CalculatorParameters.numberButtonSeven.click();
            case EIGHT -> CalculatorParameters.numberButtonEight.click();
            case NINE -> CalculatorParameters.numberButtonNine.click();
            default -> throw new IllegalStateException("Unexpected value: " + number);
        }
    }

    public static void clickSignParanLeft() {
        CalculatorParameters.buttonParanLSign.click();
    }

    public static void clickSignParanRignt() {
        CalculatorParameters.buttonParanRSign.click();
    }
}
