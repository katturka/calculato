import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;

public class Test {


    @BeforeAll
    private static void setUp() {
        CalculatorTestsConfiguration.config();
    }

    @BeforeEach
    public void clear() {
        CalculatorActions.clickClear();
    }

    @AfterAll
    private static void tearDown() {
        CalculatorTestsConfiguration.driver.quit();
    }

    @org.junit.jupiter.api.Test
    public void calculateEquationWithClickingNumbers() throws InterruptedException {
        CommonActions.checkAndClearedIfNeeded();
        CalculatorNumbersAndSignsActions.clickNumber(3);
        CalculatorNumbersAndSignsActions.clickNumber("5");
        CalculatorActions.clickMultiply();
        CalculatorNumbersAndSignsActions.clickNumber("9");
        CalculatorNumbersAndSignsActions.clickNumber("9");
        CalculatorNumbersAndSignsActions.clickNumber("9");
        CalculatorActions.clickPlus();
        CalculatorNumbersAndSignsActions.clickSignParanLeft();
        CalculatorNumbersAndSignsActions.clickNumber("1");
        CalculatorNumbersAndSignsActions.clickNumber("0");
        CalculatorNumbersAndSignsActions.clickNumber("0");
        CalculatorActions.clickDivide();
        CalculatorNumbersAndSignsActions.clickNumber("4");
        CalculatorNumbersAndSignsActions.clickSignParanRignt();
        SavedParameters.savedEquation = CalculatorParameters.input.getAttribute("value");
        CalculatorActions.clickEquals();
        Thread.sleep(2000);
        Assertions.assertEquals("34990", CalculatorParameters.input.getAttribute("value"));
    }


    @org.junit.jupiter.api.Test
    public void calculateCosPi() throws InterruptedException {
        CommonActions.checkAndClearedIfNeeded();
        CalculatorActions.clickRadian();
        CalculatorActions.clickCos();
        CalculatorActions.clickPi();
        SavedParameters.savedCosPi = CalculatorParameters.input.getAttribute("value");
        CalculatorActions.clickEquals();
        Thread.sleep(2000);

        Assertions.assertEquals("-1", CalculatorParameters.input.getAttribute("value"));
    }

    @org.junit.jupiter.api.Test
    public void calculateSqrt() throws InterruptedException {
        CommonActions.checkAndClearedIfNeeded();
        CalculatorActions.clickSqrt();
        CalculatorNumbersAndSignsActions.clickNumber("8");
        CalculatorNumbersAndSignsActions.clickNumber("1");
        SavedParameters.savedSqrt = CalculatorParameters.input.getAttribute("value");
        CalculatorActions.clickEquals();
        Thread.sleep(2000);
        Assertions.assertEquals("9", CalculatorParameters.input.getAttribute("value"));
    }

    @org.junit.jupiter.api.Test
    public void checkHistory() throws InterruptedException {
        CommonActions.clickHistory();
        Assertions.assertTrue(CommonActions.checkEquationHistory(SavedParameters.savedCosPi, SavedParameters.savedEquation, SavedParameters.savedSqrt));
    }
}
